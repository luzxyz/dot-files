# This RC contains code from others, credits:
# https://github.com/rusty-electron/dotfiles/blob/master/.bashrc

#>>>>>>>>>>>>>>>>>>
# Set up the prompt
#>>>>>>>>>>>>>>>>>>


autoload -Uz vcs_info

# VCS information output, works with fossil, git
precmd() { vcs_info }
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git:*' formats '<%s:%b(%u%c)>'


setopt PROMPT_SUBST


#PROMPT='>'
PROMPT='%S%F{green}   %* %# %F{yellow}%2d%f%F{blue} %j %? %s┫%f%f %U%F{cyan}[${vcs_info_msg_0_}]%f%u
%F{green}┗ %f'


# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Autocompletion for zsh
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# PLUGIN: fzf-tab
source ~/zshplugins/fzf-tab/fzf-tab.plugin.zsh

# Add ~bin to path
PATH="$HOME/bin:$PATH"
PATH="$PATH:$HOME/.local/bin"

# Add development binaries to path (Kotlin CLI, maven, LSP's lang servers)
PATH="$PATH:$HOME/.local/develop/lang-servers/kotlin-language-server/server/build/install/server/bin"
PATH="$PATH:$HOME/.local/develop/lang-servers/sumneko-lua/bin"

# ASDF tool manager
. $HOME/.asdf/asdf.sh

# Custom aliases
#alias ls='ls -lh --color=auto'
alias gimmehour="date +'%T'"
alias image='nsxiv'
alias leer='apvlv'
#alias IRC='irssi'
alias fzf='fzf --preview="bat {}"'
alias lsblk='lsblk -pT -o NAME,FSTYPE,UUID,MOUNTPOINT,SIZE'
alias ps='ps -A --sort -rss -o comm,pmem,rss,pid'
alias xbepis='xbps-query -l | wc -l'
alias xpepis='xbps-query -m | wc -l'
alias v='nvim'
alias cp='cp -v'
alias mv='mv -v'
alias exa='exa -lhx'
alias exg='exa --tree --git'
alias xbi="doas xbps-install"
alias xbq="xbps-query"
alias xbr="doas xbps-remove"
alias xbu="doas xbps-install -Su && notify-send -u low 'System updated!' || notify-send -u critical 'ERROR: Update'"
alias news="newsboat"
alias sudo='doas'
alias buscar='grep --color=auto'
alias h2fetch='echo "mfetch: cute ascii art, pfetch POSIX complaint fetch, onefetch Rust Fetch for projects"'
alias c='clear'

alias nfetch='neofetch --kitty /home/yorch/Pictures/wallpapers/nyami.png --crop_mode normal --size 300px --yoffset 1 --xoffset 2'

alias evirt='doas sv up docker libvirtd virtlockd virtlogd '
alias uvirt='doas sv down docker libvirtd virtlockd virtlogd '
alias xc='xsel --clipboard'
alias mcd='source /home/yorch/scripts/manycd'

# Git aliases
alias gl1l='git log --oneline'
alias gl1l='git log --oneline'
alias gpull='git pull' gremote='git remote -v' gblame='git blame'
alias gdiff='git diff' gpush='git push'
alias gwtree='git worktree'
alias trm='trash-put' trc='trash-empty' trl='trash-list'

# Start, end, supr keys 
bindkey  "^[[7~"   beginning-of-line
bindkey  "^[[8~"   end-of-line
bindkey  "^[[3~"  delete-char

# Colorful manpages 
export LESS_TERMCAP_mb=$'\e[1;31m'
export LESS_TERMCAP_md=$'\e[1;31m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;32m'

