local wezterm = require('wezterm')

local LAUNCH = require('config.launch')
local KEYS = require('config.keybinds')
local HLINKS = require('config.links')
local T = require('theme.base_suiza')

return {
   font = wezterm.font 'Monaspace Krypton',
   enable_wayland = false,
   font_size = 15,
   colors = T.colors,
   window_frame = T.window_frame,
   inactive_pane_hsb = T.inactive_pane_hsb,
   keys = KEYS,
   hyperlink_rules = HLINKS,
   default_cwd = '/home/d100',
   launch_menu = LAUNCH,
   hide_tab_bar_if_only_one_tab = true,
   --[[
   window_background_image = T.bg_image,
   window_background_image_hsb = T.bg_image_hsb,
   ]]--
   background = T.background,
}
