local wezterm = require('wezterm')
local action = wezterm.action

local keys = {
    {
      key = 't',
      mods = 'CTRL',
      action = action.SpawnTab 'CurrentPaneDomain'
    },{ 
      key = 't',
      mods = 'CTRL|ALT',
      action = action.CloseCurrentTab { confirm = false }
    },{ 
      key = 'e',
      mods = 'CTRL',
      action = action.ShowLauncher
    },{ 
      key = 'q',
      mods = 'CTRL',
      action = action.ShowTabNavigator 
    },{ 
      key = 'l',
      mods = 'ALT',
      action = action.SplitHorizontal { domain = 'CurrentPaneDomain' } 
    },{ 
      key = 'k',
      mods = 'ALT',
      action = action.SplitVertical { domain = 'CurrentPaneDomain' } 
    },{
      key = '1',
      mods = 'CTRL',
      action = action.PaneSelect {
        alphabet = 'qwertyuiop[]',
      },
    },

}

return keys
