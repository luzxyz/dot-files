local launch = {
    {
	label = 'System viewer',
        args = {'atop'}
    },{
        label = 'Pictures folder',
        cwd = '/home/yorch/Pictures'
    },{
        label = 'Newsboat',
        args = {'newsboat'}
    },{
        label = 'Develop',
        cwd = '/home/yorch/.local/develop'
    },
}

return launch
