local wezterm = require('wezterm')

local SEL = {}

-- Color definitions

local bg = '#1f2027'
local fg = '#e6eae7'

local n_black = '#2d3343'
local n_red = '#1ad2e3'
local n_green = '#eba41f'
local n_yellow = '#821832' 
local n_blue = '#145480'
local n_magenta = '#b53dfd'
local n_cyan = '#50f950'
local n_white = '#c6c6c9'

local b_black = '#7c7c7c'
local b_red = '#16fab1'
local b_green = '#be2323'
local b_yellow = '#a1193a'
local b_blue = '#2a879b'
local b_magenta = '#d715f9'
local b_cyan = '#83ff03'
local b_white = '#e8e8ec'

SEL.colors = {
    background = bg,
    foreground = fg,

    cursor_bg = fg,
    cursor_fg = bg,
    cursor_border = fg,

    selection_fg = bg,
    selection_bg = fg,

    scrollbar_thumb = fg,

    split = n_green,

    ansi = {
      n_black,
      n_red,
      n_green,
      n_yellow,
      n_blue,
      n_magenta,
      n_cyan,
      n_white,
    },

    brights = {
      b_black,
      b_red,
      b_green,
      b_yellow,
      b_blue,
      b_magenta,
      b_cyan,
      b_white,
   },

   compose_cursor = n_yellow,

   copy_mode_active_highlight_bg = { Color = fg },

   copy_mode_active_highlight_fg = { Color = bg },
   copy_mode_inactive_highlight_bg = { Color = n_black },
   copy_mode_inactive_highlight_fg = { Color = b_black },

   quick_select_label_bg = { Color = n_green },
   quick_select_label_fg = { Color = fg },
   quick_select_match_bg = { Color = b_cyan },
   quick_select_match_fg = { Color = fg }

}

SEL.inactive_pane_hsb = {
    saturation = 0.5,
    brightness = 0.5,
}

SEL.window_frame = {
    font = wezterm.font { family = 'Iosevka Nerd Font', weight = 'Bold' },
    font_size = 12.0,
    active_titlebar_bg = bg,
    inactive_titlebar_bg = bg,
}

SEL.colors.tab_bar = {
      background = '#0b0022',

      active_tab = {
        bg_color = n_green,
        fg_color = fg,
        intensity = 'Normal'
      },

      inactive_tab = {
        bg_color = bg,
        fg_color = fg
      },

      inactive_tab_hover = {
        bg_color = n_black,
        fg_color = fg
      },

      new_tab = {
        bg_color = bg,
        fg_color = fg
      },

      new_tab_hover = {
        bg_color = n_magenta,
        fg_color = fg
      },
}

-- SEL.bg_image  = '/home/yorch/.config/wezterm/theme/images/background.png'

SEL.bg_image_hsb = {
    brightness = 1.0,

    hue = 1.0,

    saturation = 1.0,
}

return SEL
