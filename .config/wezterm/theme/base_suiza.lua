local wezterm = require('wezterm')

local b_suiza = {}

-- general variables
local HOME = '/home/d100/'

-- Color definitions

local bg = '#1e1e1e'
local fg = '#d4d1d1'

local n_black = '#282a2e'
local n_red = '#9b3737'
local n_green = '#679440'
local n_yellow = '#de8344'
local n_blue = '#467ca8'
local n_magenta = '#7e4a8f'
local n_cyan = '#7ac2b9'
local n_white = '#d3dae0'

local b_black = '#373b41'
local b_red = '#cc6666'
local b_green = '#86bd68'
local b_yellow = '#f4c364'
local b_blue = '#4991ce'
local b_magenta = '#c278d8'
local b_cyan = '#8bd2c9'
local b_white = '#d3dcd6'

b_suiza.colors = {
    background = bg,
    foreground = fg,

    cursor_bg = fg,
    cursor_fg = bg,
    cursor_border = fg,

    selection_fg = bg,
    selection_bg = fg,

    scrollbar_thumb = fg,

    split = n_white,

    ansi = {
      n_black,
      n_red,
      n_green,
      n_yellow,
      n_blue,
      n_magenta,
      n_cyan,
      n_white,
    },

    brights = {
      b_black,
      b_red,
      b_green,
      b_yellow,
      b_blue,
      b_magenta,
      b_cyan,
      b_white,
   },

   compose_cursor = n_yellow,

   copy_mode_active_highlight_bg = { Color = fg },

   copy_mode_active_highlight_fg = { Color = bg },
   copy_mode_inactive_highlight_bg = { Color = n_black },
   copy_mode_inactive_highlight_fg = { Color = b_black },

   quick_select_label_bg = { Color = n_green },
   quick_select_label_fg = { Color = fg },
   quick_select_match_bg = { Color = b_cyan },
   quick_select_match_fg = { Color = fg }

}

b_suiza.inactive_pane_hsb = {
    saturation = 1.0,
    brightness = 1.0,
}

b_suiza.window_frame = {
    font = wezterm.font { family = 'Iosevka Nerd Font', weight = 'Bold' },
    font_size = 12.0,
    active_titlebar_bg = bg,
    inactive_titlebar_bg = bg,
}

b_suiza.colors.tab_bar = {
      background = bg,

      active_tab = {
        bg_color = fg,
        fg_color = bg,
      },

      inactive_tab = {
        bg_color = bg,
        fg_color = fg
      },

      inactive_tab_hover = {
        bg_color = n_black,
        fg_color = fg
      },

      new_tab = {
        bg_color = bg,
        fg_color = fg
      },

      new_tab_hover = {
        bg_color = n_magenta,
        fg_color = fg
      },
}

--[[
-- Old

b_suiza.bg_image  = HOME .. '.config/wezterm/theme/images/yotsuba.png'

b_suiza.bg_image_hsb = {
    brightness = 0.5,
    hue = 1.0,
    saturation = 0.5,
}
]]--

local imgParentDir = HOME .. '.config/wezterm/theme/images/'

local imgBgBase = imgParentDir .. 'yotsuba.png'
local imgBgA = imgParentDir .. 'yotsubaA.png'
local imgBgB = imgParentDir .. 'yotsubaB.png'
local imgBgC = imgParentDir .. 'yotsubaC.png'
local imgH = '33%'
local imgW = '23%'


b_suiza.background = {
  {
    source = {
      File = imgBgBase,
    },
    repeat_x = 'Mirror',
  },
  {
    source = {
      File = imgBgC,
    },
    width =  imgW,
    height = imgH,
    repeat_x = 'NoRepeat',
    vertical_align = 'Bottom',
    horizontal_align = 'Center',
    horizontal_offset = '30%',
    repeat_y_size = '200%',
    attachment = { Parallax = 0.1 },
  },
  {
    source = {
      File = imgBgB,
    },
    width =  imgW,
    height = imgH,
    repeat_x = 'NoRepeat',
    horizontal_align = 'Center',
    vertical_align = 'Middle',
    repeat_y_size = '200%',
    attachment = { Parallax = 0.3 },
  },
  {
    source = {
      File = imgBgA,
    },
    width =  imgW,
    height = imgH,
    repeat_x = 'NoRepeat',
    horizontal_align = 'Left',
    vertical_align = 'Top',
    vertical_offset = '10%',
    horizontal_offset = '10%',
    repeat_y_size = '200%',
    attachment = { Parallax = 0.5 },
  },
}


return b_suiza
