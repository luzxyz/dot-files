local beautiful = require("beautiful")
local awful = require("awful")

require("config.error")

local THEME = "softnwet"
beautiful.init(awful.util.getdir("config") .. "themes/".. THEME .. "/theme.lua")

-- configuration
require("config.wpaper")
require("config.layouts")
require("config.rules")
require("config.keys")

-- Standard awesome library
local gears = require("gears")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")


-- Notification library
local naughty = require("naughty")



-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()
awful.screen.connect_for_each_screen(function(s)

    -- Each screen has its own tag table.
    awful.tag({ " ", " ", " ", " "}, s, awful.layout.layouts[1])

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "right", screen = s, })


    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.ratio.vertical,
        -- left 
        {
	    valign = "center",
            halign = "center",
            layout = wibox.container.place,
	    {
              s.mytaglist,
              direction = 'west',
              widget = wibox.container.rotate
            },

        },

        -- center
	{
            layout = wibox.layout.fixed.vertical,	    
	},

        -- right
        {
            layout = wibox.layout.fixed.vertical,
	    {
		{
                  mykeyboardlayout,
                  wibox.widget.systray(),
                  layout = wibox.layout.fixed.horizontal,
	        },
              direction = 'west',
              widget = wibox.container.rotate
            },{
              mytextclock,
              direction = 'west',
              widget = wibox.container.rotate
            },
        },
    }
end)
-- }}}



-- Signals
client.connect_signal("manage", function (c)

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)


-- Shell commands
awful.spawn.with_shell("~/.config/awesome/config/autoexec.sh")
