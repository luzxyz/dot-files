local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")

local def = require("config.def")

-- Power user buttons
local modkey = def.keys.modkey
local altkey = def.keys.altkey

local keys={}

keys.globalkeys = gears.table.join(

    awful.key({altkey,}, "s",   hotkeys_popup.show_help,
              {description="show help", group="awesome"}),

    awful.key({ modkey,}, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),

    awful.key({ modkey,}, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),

    awful.key({ modkey,}, "Up", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,}, "j",
        function () awful.client.focus.byidx( 1) end,
        {description = "focus next by index", group = "client"}),

    awful.key({ modkey,           }, "k",
        function () awful.client.focus.byidx(-1) end,
        {description = "focus previous by index", group = "client"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),

    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),

    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),

    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),

    awful.key({ modkey,}, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    -- Spawn programs
    awful.key({ modkey }, "Return", function () awful.spawn(def.terminal) end,
              {description = "open Alacritty terminal", group = "terminal"}),

    awful.key({ modkey,"Shift" }, "Return", function () awful.spawn(def.terminal_2) end,
              {description = "open Wezterm terminal", group = "terminal"}),

    awful.key({ modkey }, "b", function () awful.spawn(def.ibrowser) end,
              {description = "open browser", group = "browser"}),

    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),

    awful.key({ modkey, altkey }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey }, "l",function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),

    awful.key({ modkey }, "h",function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),

    awful.key({ modkey, "Shift"}, "h",function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),

    awful.key({ modkey, "Shift"}, "l",function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),

    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),

    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),

    awful.key({ modkey }, "w", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),

    -- Execute bemenu
    awful.key({ modkey }, "p",
              function()
	      awful.spawn(def.run.launcher) end,
              {description = "show bemenu", group = "launcher"}),

    -- Print screen
    awful.key({ modkey },"Print",
             function()
		     awful.spawn.with_shell(def.run.scrot_full) end,
              {description = "Fullscreen screenshot and save it", group = "launcher"}),

    awful.key({ }, "Print",
             function()
		     awful.spawn(def.run.scrot_gui) end,
              {description = "Fullscreen gui", group = "launcher"}),

    -- lock screen
    awful.key({ altkey, "Control" }, "l",
             function()
	         awful.spawn.with_shell(def.run.slock) end,
	     {description = "Screenlocker", group="awesome"})
)


-- Bind all key numbers to tags.
for i = 1, 4 do

    keys.globalkeys = gears.table.join(keys.globalkeys,

        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),

        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),

        -- Move client to tag.
        awful.key({ modkey, altkey}, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),

        -- Toggle tag on focused client.
        awful.key({ modkey, altkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

keys.clientkeys = gears.table.join(

    awful.key({ modkey }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),

    awful.key({ modkey }, "c", function (c) c:kill() end,
              {description = "close", group = "client"}),

    awful.key({ modkey }, "s",  awful.client.floating.toggle,
              {description = "toggle floating", group = "client"})

)

keys.clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

root.keys(keys.globalkeys)

return keys
