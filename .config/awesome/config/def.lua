local def = {
   terminal = "alacritty",
   terminal_2 = "wezterm",
   ibrowser = "palemoon"
}

def.run = {
   scrot_gui = "flameshot gui",
   scrot_full = "flameshot screen -n 0 -c -p ~/Pictures/screenshots",
   launcher = "bemenu-run -l 4 -p '' -b -W 0.4 --fn 'MesloLGM Nerd Font Mono',18 -l 4",
   slock = "~/scripts/lockscreen"
}

def.keys = {
    modkey = "Mod4",
    altkey = "Mod1"
}

return def
