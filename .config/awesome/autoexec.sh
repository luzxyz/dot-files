#! /bin/sh
# Desktop configuration
exec setxkbmap -model pc104 -layout "us,ru,es" -option grp:win_space_toggle &
# start picom 
picom -bcf -o 0.34 -l -10 -t -10 -r 10 -D 50 --fade-in-step 0.2 --fade-out-step 0.27 --vsync --config ~/.config/picom.conf --corner-radius 8 &
