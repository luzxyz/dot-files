call plug#begin('~/.config/nvim/plugged')
"Some color themes
Plug 'embark-theme/vim', { 'as': 'embark', 'branch': 'main' }
Plug 'cocopon/iceberg.vim'
Plug 'danilo-augusto/vim-afterglow'
Plug 'nikolvs/vim-sunbather', {'branch': 'master'}
Plug 'goatslacker/mango.vim', {'branch': 'master'}
Plug 'owickstrom/vim-colors-paramount'
Plug 'humanoid-colors/vim-humanoid-colorscheme'
Plug 'rebelot/kanagawa.nvim'

Plug 'pgdouyon/vim-yin-yang', {'as': 'yinyang', 'branch': 'master'}
Plug 'noahfrederick/vim-hemisu', {'branch': 'master'}
Plug 'tadachs/kit.vim'

"LSP
Plug 'neovim/nvim-lspconfig'

"Web development plugins
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'alvan/vim-closetag'

"Commentaries // Needs configuration for certain file extensions
Plug 'tpope/vim-commentary'

"Tag plugins
Plug 'jiangmiao/auto-pairs'
Plug 'vim-scripts/c.vim', {'for': ['c', 'cpp']}

"Signs
Plug 'lewis6991/gitsigns.nvim'

"Navigation
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-telescope/telescope.nvim' " fuzzy finder
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }

"Specific plugins
Plug 'udalov/kotlin-vim'

"Autocompletion and Snippets
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'L3MON4D3/LuaSnip'

call plug#end()

" Editor configurations
syntax on

set t_Co=256
set termguicolors
set number nu rnu

set background=light
colorscheme kanagawa 

" disabling auto-comment by default
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Enable Disable Auto Indent
map <leader>i :setlocal autoindent<CR>
map <leader>I :setlocal noautoindent<CR>

"Actions
nnoremap <silent> ,<space> :nohlsearch<CR>
nnoremap <silent> ,nn :tabnew<CR>
nnoremap <silent> ,nm :tabclose<CR>
nnoremap <silent> ,nz :tabnext<CR>
nnoremap <silent> ,nx :tabprevious<CR>
nnoremap <silent> ,bd :set background=dark<CR>
nnoremap <silent> ,bl :set background=light<CR>

"Telescope plugin mappings
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"Hex editing
nnoremap .x :%!xxd<CR>
nnoremap .z :%!xxd -r<CR>

"Macros
"Replace All
nnoremap S :%s//gc<Left><Left>

"LSP Language servers
luafile ~/.config/nvim/lsp.lua
