#!/bin/sh
# Modifying this is a pain

BG_BASE=343437
FG_BASE=f0f0f4
FG_A=8ddfef
FG_B=f5764f

DISABLE=00000000

echo "
ignore-empty-password
show-failed-attempts
daemonize
show-keyboard-layout


inside-color=$BG_BASE
inside-clear-color=$BG_BASE
inside-caps-lock-color=$BG_BASE
inside-ver-color=$BG_BASE
inside-wrong-color=$BG_BASE

key-hl-color=$FG_A
bs-hl-color=$FG_BASE # backspace color

layout-bg-color=$BG_BASE
layout-border-color=$FG_BASE
layout-text-color=$FG_BASE

#irrelevant, line-uses-ring is in use
line-color=00ff00 
line-clear-color=ff0000
line-caps-lock-color=ff0000
line-ver-color=ff0000
line-wrong-color=ff0000

line-uses-ring
ring-color=$FG_BASE
ring-clear-color=$FG_A
ring-caps-lock-color=$FG_B
ring-ver-color=$FG_A
ring-wrong-color=$FG_B

separator-color=$DISABLE

text-color=$FG_BASE
text-clear-color=$FG_A
text-caps-lock-color=$FG_B
text-ver-color=$FG_A
text-wrong-color=$FG_B

caps-lock-bs-hl-color=$FG_A
caps-lock-key-hl-color=$FG_B

font=Sudo
font-size=52
indicator-radius=200
indicator-thickness=20
indicator-x-position=350
indicator-y-position=330
" > $HOME/.config/swaylock/config


