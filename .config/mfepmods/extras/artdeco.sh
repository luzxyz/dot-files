#!/bin/bash
echo -e "\e[0;30m■ \e[m\e[0;36m■ \e[m   .--."
echo -e "\e[0;31m■ \e[m\e[0;37m■ \e[m  / x X."
echo -e "\e[0;32m■ \e[m\e[0;30m■ \e[m  |   \e[0;33m█\e[m|"
echo -e "\e[0;33m■ \e[m\e[0;30m■ \e[m /  .__. "
echo -e "\e[0;34m■ \e[m\e[0;30m■ \e[m | /████\\"
echo -e "\e[0;35m■ \e[m\e[0;30m■ \e[m |\e[0;33m█\e[m █████ \e[0;33m█\e[m 'Pinbox'"
echo -e "\n"
